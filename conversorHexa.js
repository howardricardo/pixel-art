const converterRGBA = cor => {
    const canvas = document.createElement('canvas');
    canvas.height = 1;
    canvas.width = 1;

    const contexto = canvas.getContext('2d')
    contexto.fillStyle = cor;
    contexto.fillRect(0, 0, 1, 1);

    return contexto.getImageData(0, 0, 1, 1).data;
}

const byteToHex = num => `0${num.toString(16)}`.slice(-2);

const converterHexa = cor => {
    let rgba = converterRGBA(cor);
    let hex = [0, 1, 2].map(elemento => byteToHex(rgba[elemento])).join("")

    return `#${hex}`
}