const container = document.getElementById("container")
const button = document.getElementById("button")
const qtdPixels = document.getElementById("qtdPixels")
const corPixel = document.getElementById("corPixel")

// ------------------------------------------------------------

let corAtual = "#000000"
let corBorracha = "#FFFFFF"

const criaPixel = id => {
    const pixel = document.createElement("span")
    pixel.classList.add("pixel")

    pixel.id = id
    pixel.style.background = corPixel.value
    pixel.addEventListener('click', () => {
        pixel.style.background = corAtual
    })

    return pixel
}

const colocaPixelNaTela = (box, pixel) => { box.appendChild(pixel) }

const criaSeletorDeCor = () => {
    const inputColor = document.createElement("input")
    inputColor.type = "color"
    inputColor.id = "color"

    inputColor.addEventListener('blur', setaCorAtual)

    return inputColor
}

const pegaCor = () => document.getElementById("color").value

const colocaSeletorDeCorNaTela = (box, seletor) => { box.appendChild(seletor) }

const apaga = () => {
    corAtual = corBorracha
    const inputColor = document.getElementById("color")
    console.log(corAtual)
    inputColor.value = corAtual
}

const criaBorracha = () => {
    const borracha = document.createElement("button")
    borracha.innerHTML = "Borracha"
    borracha.addEventListener('click', apaga)

    return borracha
}

const colocaBorrachaNaTela = (box, borracha) => { box.appendChild(borracha) }

const colocaOuRemoveBorda = (tamanhoTabela, coloca) => {
    for (let i = 0; i < tamanhoTabela; i++) {
        const pixel = document.getElementById(i)
        pixel.style.width = coloca ? '8px' : '10px'
        pixel.style.height = coloca ? '8px' : '10px'
        pixel.style.border = coloca ? '1px solid rgba(0, 0, 0, 0.2)' : '0'
    }
}

const criaDinamicaDeBorda = (tamanhoTabela, colocaOuRemove) => {
    const controladorDeBorda = document.createElement("button")
    controladorDeBorda.innerHTML = colocaOuRemove ? "adicionar Bordas" : "remover Bordas"
    controladorDeBorda.addEventListener('click', () => { colocaOuRemoveBorda(tamanhoTabela, colocaOuRemove) })

    return controladorDeBorda
}

const colocaDinamizadorDeBordaNaTela = (box, dinamizador) => { box.appendChild(dinamizador) }

const reiniciaTabela = tabela => { tabela.innerHTML = "" }
const setaCorAtual = () => { corAtual = pegaCor() }

const montaTabela = qtd => {
    reiniciaTabela(container)

    const corPixelEmHexa = converterHexa(corPixel.value)

    corBorracha = corPixelEmHexa
    corAtual = "#000"

    console.log(corAtual)

    const tabelaQuadrada = qtd ** 2 
    const tamanhoTabela = tabelaQuadrada

    container.style.width = `${qtd * 10}px`
    container.style.height = `${qtd * 10}px`
    
    for (let i = 0; i < tamanhoTabela; i++) {
        const pixel = criaPixel(i)
        colocaPixelNaTela(container, pixel)
    }

    const seletor = criaSeletorDeCor()
    colocaSeletorDeCorNaTela(container, seletor)

    const borracha = criaBorracha()
    colocaBorrachaNaTela(container, borracha)

    const removedorDeBorda = criaDinamicaDeBorda(tamanhoTabela, false)
    colocaDinamizadorDeBordaNaTela(container, removedorDeBorda)

    const colocadorDeBorda = criaDinamicaDeBorda(tamanhoTabela, true)
    colocaDinamizadorDeBordaNaTela(container, colocadorDeBorda)

    if (corPixel.value === "") {
        corBorracha = "#FFFFFF"
        corAtual = "#000000"
    }
    
    corPixel.value = ""
}

button.addEventListener('click', () => {
    montaTabela(+qtdPixels.value)
})
