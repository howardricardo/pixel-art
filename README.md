# Pixel Art

- Digite o tamanho da tabela
- Digite a cor da tabela
- Selecione a cor
- Clique nos pixels para pintar

### Borracha

> A borracha sempre será da cor que você selecionou para a cor da tabela

### Bordas

> Você pode remover ou colocar as bordas para te ajudar na pixelArt

## Atenção

Se você gerar uma nova pixelArt, a antiga será sobrescrevida
